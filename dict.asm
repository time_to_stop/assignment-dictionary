extern string_equals
extern print_string

global find_word

section .text

; rdi = key
; rsi = list

%define reserved 8

find_word:

.loop:
    cmp rsi, 0
    jz .fail

    add rsi, reserved
    
    push rsi
    push rdi
    
    call string_equals
    
    pop rdi
    pop rsi

    cmp rax, 0
    jnz .success

    mov rsi, [rsi - reserved]
    jmp .loop

.success:
    sub rsi, reserved
    mov rax, rsi
    ret

.fail:
    xor rax, rax
    ret