section .bss
buffer resb 21

global exit

global string_length
global string_copy
global string_equals

global print_string
global print_newline

global print_error

global read_word

%define stdout 1
%define stderr 2

section .text

; Принимает код возврата и завершает текущий процесс
exit:
mov rax, 60
syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
mov rax, -1;

.loop:
inc rax;
cmp byte [rdi + rax], 0
jnz .loop

ret

print_string_to:
push rdi
call string_length ; rax = len(string) rdi = string
pop rdi

mov rsi, rdi ; rsi = string
mov rdx, rax; rdx = len(string)
mov rdi, rcx 
mov rax, 1
syscall
ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
mov rcx, stdout
jmp print_string_to


print_error:
mov rcx, stderr
jmp print_string_to


; Принимает код символа и выводит его в stdout
print_char:
push rdi
mov rdi, rsp

mov rsi, rsp 
mov rdx, 1
mov rdi, stdout
mov rax, 1
syscall
pop rdi

ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
mov rdi, 0xA
jmp print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
; x = rdi
cmp rdi, 0
jnz .rest
; x == 0, print("0")
mov rdi, '0'
jmp print_char

.rest:
; x != 0
push rbx
mov rbx, 10
mov rcx, 20
mov byte [buffer + rcx], 0
dec rcx
mov rax, rdi

.loop:
xor rdx, rdx
div rbx

add rdx, '0'
mov byte [buffer + rcx], dl
dec rcx

cmp rax, 0
jz .end
jmp .loop

.end:
inc rcx
add rcx, buffer
mov rdi, rcx
call print_string

pop rbx
ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
mov rax, rdi
shr rax, 63
cmp rax, 0
jz .end
push rdi
mov rdi, '-'
call print_char
pop rdi
not rdi
add rdi, 1
.end:
jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
xor rax, rax

.loop:
mov cl, byte [rdi + rax]
cmp byte [rsi + rax], cl
jnz .fail
cmp cl, 0
je .eq
inc rax
jmp .loop

.fail:
mov rax, 0
ret
.eq:
mov rax, 1
ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
push rdi
push rsi
push rcx
push 0
mov rax, 0
mov rdi, 0
mov rsi, rsp
mov rdx, 1
syscall
pop rax
pop rcx
pop rsi
pop rdi
ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
.skip:
push rdi
push rsi
call read_char
pop rsi
pop rdi 
cmp rax, 0x20
jz .skip
cmp rax, 0x9
jz .skip
cmp rax, 0xA
jz .skip
xor rcx, rcx

.loop:
cmp rax, 0x20
jz .end
cmp rax, 0x9
jz .end
cmp rax, 0xA
jz .end
cmp rax, 0
jz .end
cmp rcx, rsi
jz .fail
mov byte [rdi + rcx], al
inc rcx
push rdi
push rsi
push rcx
call read_char
pop rcx
pop rsi
pop rdi
jmp .loop

.fail:
mov rax, 0
ret
.end:
mov rax, rdi
mov rdx, rcx
mov byte[rdi + rcx], 0
ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
; rdi = input
xor rax, rax
xor rcx, rcx
xor rdx, rdx
push rbx
mov rbx, 10

.loop:
mov cl, byte [rdi + rdx]
cmp cl, '0'
jl .end
cmp cl, '9'
jg .end
sub rcx, '0'
push rdx
mul rbx
pop rdx
add rax, rcx
inc rdx
jmp .loop
.end:
pop rbx
ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
mov al, byte[rdi]
cmp al, '+'
jz .positive
cmp al, '-'
jz .negative
jmp parse_uint
.positive:
inc rdi
call parse_uint
cmp rdx, 0
jz .end
inc rdx
ret
.negative:
inc rdi
call parse_uint
cmp rdx, 0
jz .end
inc rdx
neg rax
ret
.end:
ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
push rsi
push rdi
push rdx
call string_length
pop rdx
pop rdi
pop rsi
inc rax
cmp rax, rdx

jle .continue
xor rax, rax
ret

.continue:
xor rax, rax;
.loop:
cmp rax, rdx;
jz .end
mov cl, byte [rdi + rax]
mov byte [rsi + rax], cl
inc rax
jmp .loop

.end:
mov rax, rdi
ret