%include "words.inc"

extern print_string
extern print_newline

extern print_error

extern exit

extern read_word
extern find_word

global _start


section .data

input_key: db "Enter key:", 0xA, 0
buffer_overflow_msg: db "Too large key(max - 255)" , 0xA, 0
failed_to_find: db "Target key was not found in list",  0xA, 0
success_find: db "Value for key in the list:", 0xA, 0

%define buffer_size 256
%define reserved 8

section .text

_start:
    sub rsp, buffer_size
    
    ; Greeting message
    mov rdi, input_key
    call print_string

    ; Key input

    mov rdi, rsp
    mov rsi, buffer_size
    call read_word

    cmp rax, 0
    je .buffer_overflow

    mov rdi, rsp
    mov rsi, last
    push rdx
    call find_word
    pop rdx

    cmp rax, 0
    jz .not_found

    add rax, reserved
    add rax, rdx
    inc rax

    push rax

    mov rdi, success_find
    call print_string

    pop rax

    mov rdi, rax
    call print_string
    call print_newline

    xor rdi, rdi
    add rsp, buffer_size
    call exit

.not_found:
    mov rdi, failed_to_find
    jmp .error

.buffer_overflow:
    mov rdi, buffer_overflow_msg

.error:
    call print_error

    mov rdi, 1
    add rsp, buffer_size
    call exit
